const fs = require('fs');
const path = require('path');

process.argv.splice(0, 2);
const data = process.argv.join(' ');
const dataArray = data.split(' ');

const contents = fs.readFileSync(path.join(__dirname, dataArray[0]), 'utf8');
const rainbowWords = contents.split('\n');
const password = dataArray[1];
let correctPassword = 'wrong hash';
rainbowWords.forEach(item => {
    const itemArray = item.split(" ");
    if (itemArray[1] === password) {
        correctPassword = itemArray;
    }
});
console.log(correctPassword);
fs.writeFileSync(path.join(__dirname, "/result.txt"),correctPassword[0]);

