const fs = require('fs');
const path = require('path');

const contents = fs.readFileSync(path.join(__dirname, "/pan_tadeusz.txt"), 'utf8');
const replaceSpecial = contents.replace(/[^A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]/g, " ");
const splitWords = replaceSpecial.trim().split(" ");
const uniqueWords = [];

splitWords.forEach(item => {
    if (!uniqueWords.includes(item) && item != "")
        uniqueWords.push(item);
});

const CreateFiles = fs.createWriteStream(path.join(__dirname, "/word_list.txt"));
for (let i = 0; i < uniqueWords.length; i++) {
    CreateFiles.write(uniqueWords[i].toString() + '\r\n')
}
