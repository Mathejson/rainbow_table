const fs = require('fs');
const path = require('path');
const md5 = require('js-md5');

process.argv.splice(0, 2);
let filename = process.argv.join(' ');

const contents = fs.readFileSync(path.join(__dirname, "" + filename), 'utf8');
const words = contents.split("\n");
const rainbowWords = [];
words.forEach(item => {
    let trimmedWord = item.trim();
    const tempValue = md5(trimmedWord);
    let rainbowItem = trimmedWord.concat(' ' + tempValue);
    rainbowWords.push(rainbowItem);
});
const CreateFiles = fs.createWriteStream(path.join(__dirname, "/rainbow_word_list.txt"));
for (let i = 0; i < rainbowWords.length; i++) {
    CreateFiles.write(rainbowWords[i].toString() + '\n') //'\r\n at the end of each value
}
